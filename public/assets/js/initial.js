if(typeof(Storage) !== "undefined") {
    // console.log("Local storage is supported.");
    // Local storage is available on your browser
    const username = localStorage.getItem('username');
    const score = localStorage.getItem('score');
    if (username){
        let form = document.forms["helloForm"];
        form.style.display = "none";
        let modal = document.getElementById("modal");
        let modalContent = modal.children[0].children[2];
        modal.style.display = "block";
        modalContent.innerHTML = "username: " + username + "<br>" + "score: " + score;
        let header = document.getElementById("main-header");
        header.innerHTML = "Hello " + username;
        let validateButton = document.getElementsByClassName("saved-data-accept")[0];
        let dismissButton = document.getElementsByClassName("saved-data-refusal")[0];
        validateButton.onclick = function(){
            window.location.href = "gamePlay.html";
        }
        dismissButton.onclick = function(){
            modal.style.display = "none";
            form.style.display = "block";
            localStorage.clear();
        }
    }
}



function validateForm(){
    var x = document.forms["helloForm"]["name"].value;
    if (x == "") {
        alert("I need to know your name ");
        return false;
    }
    else{
        alert("Hello " + document.forms["helloForm"]["name"].value);
        //more advanced pt2: make a system that changes the webpage based on the inputted name 
    }
    localStorage.setItem("username", x);
}
